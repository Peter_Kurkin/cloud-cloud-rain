BEGIN;
CREATE TABLE users
(
    id              INTEGER     NOT NULL,
    username        VARCHAR(64) NOT NULL,
    hashed_password VARCHAR(80) NOT NULL,
    city            VARCHAR(64) NOT NULL,
    role            INTEGER,
    PRIMARY KEY (id)
);

CREATE UNIQUE INDEX ix_users_username ON users (username);

CREATE TABLE city
(
    id        INTEGER       NOT NULL,
    name      VARCHAR(64)   NOT NULL,
    link_city VARCHAR(1000) NOT NULL,
    PRIMARY KEY (id)
);

CREATE UNIQUE INDEX ix_city_name ON city (name);

CREATE TABLE "Favorite"
(
    id       INTEGER NOT NULL,
    user_id  INTEGER,
    city_id  INTEGER,
    date_add DATETIME,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users (id),
    FOREIGN KEY (city_id) REFERENCES city (id)
);

INSERT INTO users (id, username, hashed_password, city, role)
VALUES (1,
        'Petya',
        '123',
        'Volgograd',
        1),
       (2,
        'Rustem',
        '123',
        'Kazan',
        0);

INSERT INTO city (id, name, link_city)
VALUES (1, 'Volgograd', 'http'),
       (2, 'Kazan', 'http2');

INSERT INTO Favorite (id, user_id, city_id, date_add)
VALUES (1, 1, 1, '10-10-2020'),
       (2, 1, 2, '11-10-2020'),
       (3, 2, 1, '01-10-2020'),
       (4, 2, 2, '05-10-2020');
COMMIT;
