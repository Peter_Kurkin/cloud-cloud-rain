import datetime
import random

from werkzeug.security import generate_password_hash

from accounts.models import User, Favorite
from app import app
from company_management.models import City
from project_dataase.database import db


def random_sl():
    a = 'qwertyuiopasdfghjklzxcvbnm'
    lst = []
    l = ''
    sl = ''
    for i in range(1000):
        for x in range(64):
            l = a[random.randint(0, 25)]
            sl += l
        lst.append(sl)
        sl = ''

    return lst


def main():
    with app.app_context():
        db.create_all()
        db.session.add(
            User(username='Rustem', hashed_password=generate_password_hash('123456'), role=0, city='Kazan'))
        db.session.add(User(username='Artur', hashed_password=generate_password_hash('123456'), role=0, city='Kazan'))
        db.session.add(
            User(username='Petya', hashed_password=generate_password_hash('123'), role=1, city='Volgograd'))
        db.session.add(City(name='Москва', link_city='https://www.gismeteo.ru/weather-moscow-4368/10-days/'))
        db.session.add(City(name='Казань', link_city='https://www.gismeteo.ru/weather-kazan-4364/10-days/'))
        db.session.add(
            City(name='Екатеринбург', link_city='https://www.gismeteo.ru/weather-yekaterinburg-4517/10-days/'))
        db.session.add(
            City(name='Волгоград', link_city='https://www.gismeteo.ru/weather-volgograd-5089/10-days/'))
        db.session.add(
            City(name='Пермь', link_city='https://www.gismeteo.ru/weather-perm-4476/10-days/'))
        db.session.add(
            City(name='Санкт-Петербург', link_city='https://www.gismeteo.ru/weather-sankt-peterburg-4079/10-days/'))
        db.session.add(
            City(name='Астрахань', link_city='https://www.gismeteo.ru/weather-astrakhan-5130/10-days/'))
        db.session.add(
            City(name='Кисловодск', link_city='https://www.gismeteo.ru/weather-kislovodsk-5237/10-days/'))
        db.session.add(
            City(name='Пятигорск', link_city='https://www.gismeteo.ru/weather-pyatigorsk-5225/10-days/'))
        db.session.add(
            City(name='Ростов-на-Дону', link_city='https://www.gismeteo.ru/weather-rostov-na-donu-5110/10-days/'))
        db.session.add(
            City(name='Волжский', link_city='https://www.gismeteo.ru/weather-volzhsky-11934/10-days/'))
        db.session.add(
            City(name='Саратов', link_city='https://www.gismeteo.ru/weather-saratov-5032/10-days/'))
        db.session.add(
            City(name='Воронеж', link_city='https://www.gismeteo.ru/weather-voronezh-5026/10-days/'))

        db.session.add(Favorite(user_id=3, city_id=3, date_add=datetime.datetime.now()))
        # lst_1000 = random_sl()
        # for i in range(1000):
        #     try:
        #         db.session.add(User(username=lst_1000.pop(), hashed_password=generate_password_hash(f'{random.randint(10, 50)}'), role='0', city='Казань'))
        #     except Exception as e:
        #         pass
        db.session.commit()


if __name__ == '__main__':
    main()
