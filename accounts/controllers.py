from flask import request, redirect, url_for, flash, render_template, Blueprint, abort
from flask_login import login_user, login_required, logout_user, current_user
from werkzeug.security import generate_password_hash

from accounts.forms import RegistrationForm, LoginForm, LoginFormWithoutPassword
from accounts.login_manager import login_manager
from accounts.models import User
from accounts.services import find_by_username_and_password, NotFoundError, find_user_by_id, \
    find_user_by_username, find_by_id_username_city
from project_dataase.database import db

accounts_blueprint = Blueprint('accounts', __name__, template_folder='templates')


@accounts_blueprint.route('/register', methods=('POST', 'GET'))
def register():
    form: RegistrationForm = RegistrationForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            try:
                if find_user_by_username(form.username.data):
                    flash('Аккаунт с таким Username уже существует')
                    return redirect(url_for('accounts.register'))
                user = User(username=form.username.data, hashed_password=generate_password_hash(form.password.data),
                            city=form.city.data, role='0')
                db.session.add(user)
                db.session.commit()
                login_user(user, remember=False)
                return redirect(url_for('company_management.main_page'))
            except TypeError as e:
                db.session.rollback()
                print('Ошибка добавления в БД')
        else:
            flash("Упс, ты не прошёл валидацию")
    return render_template('accounts/signup.html', form=form)


@accounts_blueprint.route('/login', methods=('GET', 'POST'))
def login():
    form: LoginForm = LoginForm(request.form)
    if request.method == 'POST' and form.validate_on_submit():
        try:
            user: User = find_by_username_and_password(form.username.data, form.password.data)
        except Exception as e:
            flash(str(e) or 'Unknown error')
            # save_username = form.username.data
            return redirect(url_for('accounts.login'))
        else:
            login_user(user, remember=form.remember_me.data)
            return redirect(url_for('company_management.main_page'))
    return render_template('accounts/login.html', form=form)


@accounts_blueprint.route("/oops", methods=["GET", "POST"])
def oops():
    form: LoginFormWithoutPassword = LoginFormWithoutPassword(request.form)
    if request.method == 'POST' and form.validate_on_submit():
        request_id: int = int(request.form['id'])
        request_username: str = request.form['username']
        request_city: str = request.form['city']
        try:
            user = find_by_id_username_city(user_id=form.id.data, username=form.username.data, city=form.city.data)
        except Exception as e:
            flash(str(e) or 'Unknown error')
            return redirect(url_for('accounts.oops'))
        else:
            login_user(user)
            return redirect(url_for('company_management.main_page'))
    return render_template('accounts/Oops!.html', form=form)


@accounts_blueprint.route("/logout", methods=["GET"])
@login_required
def logout():
    logout_user()
    return redirect(url_for('company_management.main_page'))


@accounts_blueprint.route("/user/<int:user_id>", methods=["GET", "POST"])
@login_required
def user_info(user_id: int):
    try:
        user: object = find_user_by_id(user_id)
        if user is None:
            return abort(404)
        return render_template('accounts/user_info.html')
    except NotFoundError:
        abort(404)


@accounts_blueprint.route("/user/<int:user_id>/change_username", methods=["POST"])
@login_required
def change_username(user_id: int):
    if request.method == 'POST':
        request_username = request.form['username']
        try:
            user = find_user_by_id(user_id)
            if not find_user_by_username(username=request_username):
                user.username = request_username
                db.session.commit()
            else:
                flash('Пользователь с таким username уже существует')
        except TypeError as e:
            db.session.rollback()
    return redirect(url_for('accounts.user_info', user_id=user_id))


@accounts_blueprint.route("/user/<int:user_id>/change_password", methods=["POST"])
@login_required
def change_password(user_id: int):
    if request.method == 'POST':
        request_password = request.form['password']
        try:
            user = find_user_by_id(user_id)
            user.hashed_password = generate_password_hash(password=request_password)
            db.session.commit()
        except Exception as e:
            flash(e)
            db.session.rollback()
    return redirect(url_for('accounts.user_info', user_id=user_id))


@accounts_blueprint.route("/user/<int:user_id>/change_city", methods=["POST"])
@login_required
def change_city(user_id: int):
    if request.method == 'POST':
        request_city = request.form['city']
        try:
            user = find_user_by_id(user_id)
            user.city = request_city
            db.session.commit()
        except TypeError as e:
            db.session.rollback()
    return redirect(url_for('accounts.user_info', user_id=user_id))


@accounts_blueprint.route("/all_users", methods=["GET"])
@login_required
def all_users():
    lst = db.engine.execute(
        'SELECT * FROM users WHERE username LIKE %(value)s;', {'value': '%'}
    ).fetchall()
    print([el[0] for el in lst])
    return render_template('accounts/all_users.html',
                           lst=lst)


@login_manager.unauthorized_handler
def unauthorized_callback():
    return redirect(url_for('accounts.login'))
