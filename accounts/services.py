from werkzeug.security import check_password_hash

from accounts.models import User


class NotFoundError(Exception):
    pass


class IncorrectPasswordError(Exception):
    pass


def find_by_username_and_password(username: str, password: str):
    user = User.query.filter_by(username=username).first()
    if not user:
        raise NotFoundError('Такого username не существует')
    if not check_password_hash(user.hashed_password, password):
        raise IncorrectPasswordError('Неправильный пароль')
    return user


def find_by_id_username_city(user_id: int, username: str, city: str) -> object:
    user = User.query.filter_by(id=user_id).first()
    if not user:
        raise NotFoundError('Не существует пользователя с таким id')
    if user.username != username:
        raise NotFoundError(f'Не существует пользователя с таким username')
    if user.city != city:
        raise NotFoundError('Не существует пользователя с таким городом')
    return user


def find_user_by_id(user_id: int):
    user = User.query.filter_by(id=user_id).first()
    if not user:
        raise NotFoundError()
    return user


def find_user_by_username(username: str):
    user = User.query.filter_by(username=username).first()
    if not user:
        return False
    return True
