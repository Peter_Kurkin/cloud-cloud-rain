from datetime import datetime

from flask_login import UserMixin

from project_dataase.database import db


class User(UserMixin, db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), index=True, unique=True, nullable=False)
    hashed_password = db.Column(db.String(255), unique=False, nullable=False)
    city = db.Column(db.String(255), unique=False, nullable=False)
    role = db.Column(db.Integer)

    def __repr__(self):
        return f"<User {self.username}>"


class Favorite(UserMixin, db.Model):
    __tablename__ = "Favorite"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    city_id = db.Column(db.Integer, db.ForeignKey('city.id'))
    date_add = db.Column(db.DateTime, default=datetime.now())

    def __repr__(self):
        return f"User:{self.user_id} -> City:{self.city_id}"
