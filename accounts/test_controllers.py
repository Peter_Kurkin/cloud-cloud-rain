import pytest
from flask import url_for
from flask_login import current_user


def test_current_user_info_for_logged_in_should_be_ok(logged_in_client):
    res = logged_in_client.get(url_for('accounts.user_info', user_id=current_user.id))
    assert res.status_code == 200


def test_current_user_info_for_logged_out_should_be_ok(client):
    res = client.get(url_for('accounts.user_info', user_id=1), follow_redirects=True)
    assert res.request.path == url_for('accounts.login')


def test_current_user_info_for_not_user_id(logged_in_client):
    res = logged_in_client.get(url_for('accounts.user_info', user_id=100000), follow_redirects=True)
    assert res.status_code == 200
    assert b'404' in res.data


def test_oops(client):
    res = client.get(url_for('accounts.oops'))
    assert res.status_code == 200
    assert b'Ooops!' in res.data


@pytest.mark.parametrize('name', [
    'sdjlfn', 'gsfgsdfkjlg', 'jtrjjfhg'
])
def test_change_username(logged_in_client, name):
    res = logged_in_client.post(url_for('accounts.change_username', user_id=current_user.id),
                                data=dict(username=name), follow_redirects=True)
    assert res.status_code == 200
    assert res.request.path == url_for('accounts.user_info', user_id=current_user.id)


@pytest.mark.parametrize('password', [
    'sdjlfn', 'gsfgsdfkjlg', 'jtrjjfhg'
])
def test_change_password(logged_in_client, password):
    res = logged_in_client.post(url_for('accounts.change_password', user_id=current_user.id),
                                data=dict(password=password), follow_redirects=True)
    assert res.status_code == 200
    assert res.request.path == url_for('accounts.user_info', user_id=current_user.id)


@pytest.mark.parametrize('city', [
    'Пермь', 'Волгоград', 'Воронеж'
])
def test_change_city(logged_in_client, city):
    res = logged_in_client.post(url_for('accounts.change_city', user_id=current_user.id),
                                data=dict(city=city), follow_redirects=True)
    assert res.status_code == 200
    assert res.request.path == url_for('accounts.user_info', user_id=current_user.id)


def test_register(client):
    res = client.post(url_for('accounts.register'),
                      data=dict(username='petrrr', password='123', city='Москва', repeat_password='123'),
                      follow_redirects=True)
    assert res.status_code == 200


def test_register_no_valid(client):
    res = client.post(url_for('accounts.register'), data=dict(username='АВЫЫА', password='123', city='Москва'),
                      follow_redirects=True)
    assert res.status_code == 200


def test_register_have_account(client):
    res = client.post(url_for('accounts.register'),
                      data=dict(username='Petya', password='123', city='Москва', repeat_password='123'),
                      follow_redirects=True)
    assert res.status_code == 200
    assert res.request.path == url_for('accounts.register')


def test_login_get(client):
    assert client.get(url_for('accounts.login')) == 200


def test_login_fail(client):
    res = client.post(url_for('accounts.login'), data=dict(username='Rustem', password='sdfg'), follow_redirects=True)
    assert res.status_code == 200
    assert res.request.path == url_for('accounts.login')


def test_register_get(client):
    assert client.get(url_for('accounts.register')) == 200


def test_oops_post(client):
    res = client.post(url_for('accounts.oops'), data=dict(id='2', username='Petya', city='Волгоград'),
                      follow_redirects=True)
    assert res.status_code == 200
    assert res.request.path == url_for('company_management.main_page')


def test_oops_post_fail(client):
    res = client.post(url_for('accounts.oops'), data=dict(id='1', username='sfdsf', city='Волгоград'),
                      follow_redirects=True)
    assert res.status_code == 200
    assert res.request.path == url_for('accounts.oops')

# def test_all_users(logged_in_client):
#     res = logged_in_client.get(url_for('accounts.all_users'))
#     assert res.status_code == 200
