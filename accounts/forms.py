from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, validators, IntegerField, BooleanField


class RegistrationForm(FlaskForm):
    username = StringField('username', [
        validators.Length(min=1, max=255),
        validators.DataRequired()
    ])
    password = PasswordField('password', [validators.DataRequired(),
                                          validators.Length(min=1, max=255)
                                          ])
    city = StringField('city', [
        validators.Length(min=1, max=255),
        validators.DataRequired()
    ])
    repeat_password = PasswordField('repeat_password', [
        validators.DataRequired(),
        validators.Length(min=1, max=255)
    ])


class LoginForm(FlaskForm):
    username = StringField('username', [
        validators.Length(min=1, max=255),
        validators.DataRequired()
    ])
    password = PasswordField('password', [validators.DataRequired(),
                                          validators.Length(min=1, max=255)
                                          ])
    remember_me = BooleanField('remember_me')


class LoginFormWithoutPassword(FlaskForm):
    id = IntegerField('id', [
        validators.DataRequired()
    ])
    username = StringField('username', [
        validators.Length(min=1, max=255),
        validators.DataRequired()
    ])
    city = StringField('city', [
        validators.Length(min=1, max=255),
        validators.DataRequired()
    ])
