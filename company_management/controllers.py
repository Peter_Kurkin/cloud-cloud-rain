import json

from flask import Blueprint, render_template, request, flash, redirect, url_for
from flask_login import current_user, login_required

from accounts.models import Favorite
from accounts.services import find_user_by_id
from company_management.api import visual_crossing_weather, open_weather_map
from company_management.forms import FeedBack
from company_management.models import City
from company_management.parse import gismeteo
from project_dataase.database import db

company_management_blueprint = Blueprint('company_management', __name__, template_folder='templates')


@company_management_blueprint.route('/', methods=('GET', 'POST'))
def main_page():
    city = 'Москва'
    l_city: str = 'https://www.gismeteo.ru/weather-moscow-4368/10-days/'
    sql = City.query.all()
    dictionary = {}
    for user in sql:
        dictionary[user.name] = user.link_city
    if request.method == 'POST':
        city = request.form['city']
        f_city: City = City.query.filter_by(name=city).first()
        l_city = f_city.link_city
    else:
        f_city: City = City.query.filter_by(name=city).first()
    if current_user.is_authenticated:
        user = find_user_by_id(current_user.id)
        if Favorite.query.filter_by(city_id=City.query.filter_by(name=city).first().id,
                                    user_id=current_user.id).first():
            check_city = True
        else:
            check_city = False
        return render_template('company_management/main_page.html', city=city,
                               dictionary=json.dumps(dictionary), massiv=list(gismeteo(l_city).items()),
                               f_city=f_city.id, check_city=check_city, api_1=visual_crossing_weather(city),
                               api_2=open_weather_map(city))

    return render_template('company_management/main_page.html', city=city, dictionary=json.dumps(dictionary),
                           massiv=list(gismeteo(l_city).items()), f_city=f_city.id, api_1=visual_crossing_weather(city),
                           api_2=open_weather_map(city))


@company_management_blueprint.route('/info', methods=["GET"])
def info():
    # if current_user.is_authenticated:
    #     return render_template('company_management/info.html', user=find_user_by_id(current_user.id))
    return render_template('company_management/info.html')


@company_management_blueprint.route('/review', methods=["GET", "POST"])
@login_required
def review() -> object:
    form: FeedBack = FeedBack(request.form)
    if request.method == "POST":
        if form.validate_on_submit():
            flash('Ваш отзыв отправлен! Спасибо за обратную связь!')
            return redirect(url_for('company_management.review'))
        else:
            flash('Ваши поле(я) не прошли валидацию')
            return redirect(url_for('company_management.review'))
    return render_template('company_management/review.html', form=form)


@company_management_blueprint.route('/favorite', methods=["GET"])
@login_required
def favorite() -> object:
    lst_favorite_object: object = Favorite.query.filter_by(user_id=current_user.id).all()
    lst_city_id = [x.city_id for x in lst_favorite_object]
    lst_city_object: list[City] = []
    for city_id in lst_city_id:
        lst_city_object.append(City.query.filter_by(id=city_id).first())
    return render_template('company_management/favorite.html',
                           lst=lst_city_object)


@company_management_blueprint.route('/favorite/delete_favorite_from_favorite/<int:f_city>')
@login_required
def delete_favorite_from_favorite(f_city: int):
    try:
        Favorite.query.filter_by(user_id=current_user.id, city_id=f_city).delete()
        db.session.commit()
    except TypeError as e:
        flash('Ошибка добавления в БД')
    return redirect(url_for('company_management.favorite'))


@company_management_blueprint.route('/change_favorite/<int:f_city>')
@login_required
def add_favorite(f_city: int):
    try:
        favorite_city: Favorite = Favorite(user_id=current_user.id, city_id=f_city)
        db.session.add(favorite_city)
        db.session.commit()
    except TypeError as e:
        db.session.rollback()
        flash('Ошибка добавления в БД')
    return redirect(url_for('company_management.main_page'))


@company_management_blueprint.route('/delete_favorite/<int:f_city>')
@login_required
def delete_favorite(f_city: int):
    try:
        Favorite.query.filter_by(user_id=current_user.id, city_id=f_city).delete()
        db.session.commit()
    except TypeError as e:
        flash('Ошибка добавления в БД')
    return redirect(url_for('company_management.main_page'))
