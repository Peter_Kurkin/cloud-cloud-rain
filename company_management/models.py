from flask_login import UserMixin

from project_dataase.database import db


class City(UserMixin, db.Model):
    __tablename__ = "city"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True, nullable=False)
    link_city = db.Column(db.String(1000), unique=False, nullable=False)

    def __repr__(self):
        return f"<User {self.name}>"
