import datetime

import requests
from bs4 import BeautifulSoup


def gismeteo(city='https://www.gismeteo.ru/weather-kazan-4364/10-days/'):
    # https: // www.gismeteo.ru / weather - kazan - 4364 / 10 - days /
    url = f'{city}'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                      '(KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36'
    }
    response = requests.get(url, headers=headers)
    soup = BeautifulSoup(response.content, 'html.parser')
    date = soup.find_all('span', class_='w_date__date black')
    temp = soup.find_all('div', class_='value')

    # 💪💪💪 Strong Python 💪💪💪
    characteristic = [text['data-text'] for text in
                      [span.find('span') for span in soup.find_all('div', {'class': 'widget__value w_icon'})]]

    i = 0
    date_day = []
    while i < 7:
        date_day.append((datetime.datetime.now() + datetime.timedelta(days=i)).strftime('%d'))
        i += 1

    tmp = [div.find('span', class_='unit unit_temperature_c') for div in temp]
    i = 0
    temp_day = []
    while i < 7:
        temp_day.append(tmp[i].text)
        i += 1

    temperature_night = [div.find('div', class_='mint') for div in temp]
    temp_night = []
    i = 0
    while i < 7:
        temp_night.append((temperature_night[i].find('span', class_='unit unit_temperature_c')).text)
        i += 1

    dictionary = {}

    for index, body in enumerate(date_day):
        dictionary[date_day[index]] = [temp_day[index], temp_night[index], characteristic[index]]

    return dictionary


if __name__ == '__main__':
    print(gismeteo())
