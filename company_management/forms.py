from flask_wtf import FlaskForm
from wtforms import StringField, validators


class FeedBack(FlaskForm):
    name = StringField('name', [
        validators.Length(min=1, max=255),
        validators.DataRequired()
    ])
    email = StringField('email', [validators.DataRequired(),
                                  validators.Length(min=1, max=255)
                                  ])
    message = StringField('message', [
        validators.Length(min=1, max=10000),
        validators.DataRequired()
    ])
