from flask import url_for


def test_app(client):
    assert client.get(url_for('company_management.main_page')).status_code == 200


def test_review_get(logged_in_client):
    assert logged_in_client.get(url_for('company_management.review')).status_code == 200


# def test_review_post(logged_in_client):
#     res = logged_in_client.post(
#         url_for('company_management.review', data=dict(name='Rustem', email='12@12', message='ыварыпдурптук'),
#                 follow_redirects=True))
#     assert res.status_code == 200
#     assert res.request.path == url_for('company_management.review')


def test_review_post_no_valid(logged_in_client):
    res = logged_in_client.post(
        url_for('company_management.review', data=dict(name='Rustem', email='12@12'),
                follow_redirects=True))
    assert res.status_code == 302


def test_info(client):
    assert client.get(url_for('company_management.info')) == 200


def test_favorite_get(logged_in_client):
    assert logged_in_client.get(url_for('company_management.favorite'))


def test_add_favorite(logged_in_client):
    res = logged_in_client.get(url_for('company_management.add_favorite', f_city=1), follow_redirects=True)
    assert res.status_code == 200
    assert res.request.path == url_for('company_management.main_page')


def test_delete_favorite(logged_in_client):
    res = logged_in_client.get(url_for('company_management.delete_favorite', f_city=1), follow_redirects=True)
    assert res.status_code == 200
    assert res.request.path == url_for('company_management.main_page')
