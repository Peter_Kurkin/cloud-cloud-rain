import datetime
import http.client
import json
import re

from googletrans import Translator


# https://rapidapi.com/community/api/open-weather-map
def open_weather_map(city='Москва'):
    city = Translator().translate(city, src='ru', dest='en').text
    conn = http.client.HTTPSConnection("community-open-weather-map.p.rapidapi.com")

    headers = {
        'x-rapidapi-host': "community-open-weather-map.p.rapidapi.com",
        'x-rapidapi-key': "7baa7514a7msh26a02ec78ffa43ep14b34cjsna955ceb575e5"
    }

    conn.request("GET", f"/forecast/daily?q={city}&lat=35&lon=139&cnt=10&units=metric%20or%20imperial", headers=headers)

    res = conn.getresponse()
    data = res.read()

    data = json.loads(data.decode("utf-8"))
    forecast = {}
    i = 0
    for weather in data['list']:
        forecast[re.sub('[0]', '', ((datetime.date.today() + datetime.timedelta(days=i)).strftime('%d')))] = [
            round(weather['temp']['day'] - 273), round(weather['temp']['night'] - 273),
            weather['weather'][0]['description']]
        i += 1

    conditions = []
    for x in forecast.values():
        conditions.append(x)
    # print(conditions)
    conditions[0][0] = (conditions[0][0] + conditions[0][1]) / 2
    return conditions
    # число - погода день, погода ночь, свойство
    # TODO: По текущему дню выводит день, ночь. Хотя нужно лишь текущую температуру


# https://rapidapi.com/visual-crossing-corporation-visual-crossing-corporation-default/api/visual-crossing-weather
def visual_crossing_weather(city='Москва'):
    city = Translator().translate(city, src='ru', dest='en').text
    conn = http.client.HTTPSConnection("visual-crossing-weather.p.rapidapi.com")

    headers = {
        'x-rapidapi-host': "visual-crossing-weather.p.rapidapi.com",
        'x-rapidapi-key': "7baa7514a7msh26a02ec78ffa43ep14b34cjsna955ceb575e5"
    }

    conn.request("GET",
                 f"/forecast?aggregateHours=24&location={city}&contentType=json&unitGroup=uk&shortColumnNames=false",
                 headers=headers)

    res = conn.getresponse()
    data = json.loads((res.read()).decode('utf-8'))

    dictionary = {}

    i = 0
    for lst in data['locations'][city]['values']:
        dictionary[re.sub('[0]', '', ((datetime.date.today() + datetime.timedelta(days=i)).strftime('%d')))] = [
            lst['maxt'], lst['mint'], lst['conditions']]
        i += 1
    dictionary[re.sub('[0]', '', (datetime.date.today().strftime('%d')))] = [
        data['locations'][city]['currentConditions']['temp'],
        data['locations'][city]['currentConditions']['icon']]

    conditions = []
    for x in dictionary.values():
        conditions.append(x)
    return conditions
    # TODO: Выводит больше 7 дней
    # день(текущий) - температура(сейчас), свойство
    # день(остальное) - температура днём, температура ночью, свойство


if __name__ == '__main__':
    open_weather_map()
    visual_crossing_weather()
