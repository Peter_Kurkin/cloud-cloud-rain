import datetime

import pytest
from flask import url_for
from werkzeug.security import generate_password_hash

import app as app_module
from accounts.models import User, Favorite
from company_management.models import City


@pytest.fixture
def app():
    app_module.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
    app_module.app.config['WTF_CSRF_ENABLED'] = False
    with app_module.app.app_context():
        app_module.db.create_all()

        app_module.db.session.add(
            User(id=2, username='Petya', hashed_password=generate_password_hash('123'), city='Волгоград', role=1))

        app_module.db.session.add(City(name='Москва', link_city='https://www.gismeteo.ru/weather-moscow-4368/10-days/'))

        app_module.db.session.add(Favorite(user_id=3, city_id=3, date_add=datetime.datetime.now()))
        app_module.db.session.commit()
    yield app_module.app
    with app_module.app.app_context():
        app_module.db.session.remove()
        app_module.db.drop_all()


@pytest.fixture
def logged_in_client(client):
    app_module.db.session.add(
        User(id=1, username='Rustem', hashed_password=generate_password_hash('123456'), city='Казань', role=1))
    app_module.db.session.commit()
    client.post(url_for('accounts.login'), data=dict(username='Rustem', password='123456'))
    yield client
    client.get(url_for('accounts.logout'))
