[![pipeline status](https://gitlab.com/Peter_Kurkin/cloud-cloud-rain/badges/main/pipeline.svg)](https://gitlab.com/Peter_Kurkin/cloud-cloud-rain/-/commits/main)
[![coverage report](https://gitlab.com/Peter_Kurkin/cloud-cloud-rain/badges/main/coverage.svg)](https://gitlab.com/Peter_Kurkin/cloud-cloud-rain/-/commits/main)
# Cloud-cloud-rain

### _Для работы требуется подключение к интернету_

## Быстрый старт

1. Клонируйте репозиторий
   1. Для http:
        > git clone https://gitlab.com/Peter_Kurkin/cloud-cloud-rain.git
   2. Для ssh:
        > git clone git@gitlab.com:Peter_Kurkin/cloud-cloud-rain.git

2. Создайте виртуальное окружение внутри проекта (далее все команды надо будет делать внутри venv)
    1. Для poetry
        1. Внутри проекта напишите. Благодаря первой команде, вы укажите poetry создание папки с пакетами внутри проекта, благодаря второй команде, он создаст виртуальное окружение "poetry shell" и установит пакеты
            > poetry config virtualenvs.in-project true
            > 
            > poetry install
    2. Для нормальных людей любящих pip. 
        1. Внутри проекта напишите. Благодаря первой команде вы создадите виртуальное pip окружение. Перейдя по второй команде, вы активируете его внутри терминала
            > python3 -m venv venv
            >
            > cd venv\Scripts\activate.bat
        2. Создайте внутри проекта файл requirements.txt с содержимым:
            ``` 
           Flask==2.0.1
           Flask-Login==0.5.0
           Flask-SQLAlchemy==2.5.1
           gunicorn==20.1.0
           request2==0.2
           beautifulsoup4==4.10.0
           Flask-Migrate==3.1.0
           psycopg2-binary==2.9.1
           WTForms==2.3.3
           Flask-WTF==0.15.1
           googletrans==4.0.0-rc1
           pytest-flask==1.2.0
           pytest-cov==3.0.0
            ```
        3. Напишите следующую команду, тем самым установив пакеты в своё виртуальное окружение.
            > pip install -r requirements.txt
3. Подготовьте БД
    1. PostgreSQL
        1. Создать БД, зайдя в свой Postgres
           > psql -U (username) [default=postgres]
           > 
           > CREATE DATABASE (name_database); [default=cloud-cloud-rain]
        2. Настроить подключение. Зайти в файл local_configs.py и изменить 5 строчку по след шаблону:
            > postgresql://(username):(password)@localhost:5432/(name_database)
    2. SQLite
        1. Зайти в файл local_configs.py раскомментировать 4 строчку и убрать - 5.
4. Выполнить миграции
   1. Написать в терминале:
        > flask db upgrade
5. Заполнить БД минимальным количеством городов
    1. Запустить программу project_dataase/init_database.py
6. Запускаем проект!
    1. Напишите в терминале следующую команду, после чего появится ссылка, по которой вы сможете перейти:
         > flask run

## Прогон тестов

>pytest --cov

![img.png](for_readme/img.png)

## Пример работы

![img.png](for_readme/img_1.png)
![img.png](for_readme/img_4.png)
![img_2.png](for_readme/img_2.png)
![img_3.png](for_readme/img_3.png)
